//
//  LSOPet.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Pet;

@interface LSOPet : NSObject

-(void) loadPets;
-(NSUInteger)count;
-(Pet *)petAtIndex:(NSUInteger) index;
-(void)removeObjectAtIndex:(NSUInteger)index;
-(void)addPet:(Pet *)pet;




@property (assign) BOOL dataLoaded;
@property (assign) NSInteger statusCode;

@end

//
//  LSOPet.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "LSOPet.h"
#import "Pet.h"

@interface LSOPet ()
@property (strong) NSMutableArray *petsArray;

@end

@implementation LSOPet

- (id)init
{
    self = [super init];
    if (self) {
        _petsArray = [NSMutableArray array];
    }
    return self;
}

-(NSUInteger)count{
    return [self.petsArray count];
}

-(Pet *)petAtIndex:(NSUInteger)index
{
    return [self.petsArray objectAtIndex:index];
}

-(void)removeObjectAtIndex:(NSUInteger)index
{
    
    Pet* petToDelete = [self petAtIndex:index];
    [self.petsArray removeObjectAtIndex:index];
    NSLog(@"Deleting pet %@ -%d",petToDelete.petName,petToDelete.petId);
    NSString *deleteUrl = [NSString stringWithFormat:@"http://localhost:8080/service/pet/%d",petToDelete.petId];
    NSURL *url = [NSURL URLWithString:deleteUrl];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"DELETE";
    NSOperationQueue *dataQueue = [[NSOperationQueue alloc]init];
    dataQueue.name = @"DeleteQueue";
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:dataQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               //                        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                               //                            self.dataLoaded = YES;                    }];
                               
                           }];
    
    
}

-(void)loadPets
{
    self.dataLoaded = NO;
    NSLog(@"Loading pets:");
    //NSURL *url = [NSURL URLWithString:@"http://teachersdoor.com/json-bigpets.js"] ;
    NSURL *url = [NSURL URLWithString:@"http://localhost:8080/service/pet"] ;
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // Does the work on the main thread (queue)
    //    [NSURLConnection sendAsynchronousRequest:request
    //                                       queue:[NSOperationQueue mainQueue]
    //                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    NSOperationQueue *dataQueue = [[NSOperationQueue alloc]init];
    dataQueue.name = @"LoadQueue";
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:dataQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         
         if ([data length] >0 && error == nil)
         {
             
             NSError *jsonError;
             NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&jsonError];
             [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *jsonDictionary,
                                                     NSUInteger idx,
                                                     BOOL *stop)
              {
                  Pet *pet = [[Pet alloc] initWithJSONDictionary:jsonDictionary];
                  [self.petsArray addObject:pet];
              }];
             NSLog(@"Number of elements is %d",[self.petsArray count]);
             
             NSSortDescriptor *nameSorter = [NSSortDescriptor sortDescriptorWithKey:@"petName" ascending:YES];
             [self.petsArray sortUsingDescriptors:@[nameSorter]];
             
             [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                 self.dataLoaded = YES;
             }];
         }
         else if ([data length] == 0 && error == nil)
         {
             NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             NSLog(@"Error = %@", error);
         }
       
     }];
    
    
    NSLog(@"After sendAsynchronousRequest:");
}

-(void)addPet:(Pet *)pet {
    
    NSLog(@"%@",[pet description]);
    NSLog(@"Adding pet %@",pet.petName);
    NSString *postURL = [NSString stringWithFormat:@"http://localhost:8080/service/pet"];
    NSURL *url = [NSURL URLWithString:postURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSError *jsonError;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[pet jsonDictionary]
                                                   options:NSJSONReadingMutableContainers
                                                     error:&jsonError];
    request.HTTPBody = data;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"fr" forHTTPHeaderField:@"Accept-Language"];
    
    
    NSOperationQueue *dataQueue = [[NSOperationQueue alloc] init];
    dataQueue.name = @"PostQueue";
    self.statusCode = 200;
    
    NSHTTPURLResponse *response;
    NSError *error;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
                                    self.statusCode = [response statusCode];
                                   NSLog(@"Status code is %d",[response statusCode]);
                                   NSLog(@"Error = %@", error);
    
    if (self.statusCode >= 400) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"A network error has occurred"
                                                          message:[NSString stringWithFormat:@"Error code %d",self.statusCode]
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }
    else {
        [self.petsArray addObject:pet];
        NSSortDescriptor *nameSorter = [NSSortDescriptor sortDescriptorWithKey:@"petName" ascending:YES];
        [self.petsArray sortUsingDescriptors:@[nameSorter]];
        self.dataLoaded = YES;
    }
}


@end

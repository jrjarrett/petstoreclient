//
//  LSOEditPetCell.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "LSOEditPetCell.h"

@interface LSOEditPetCell() <UITextFieldDelegate>



@end


@implementation LSOEditPetCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"\n\tFunction\t=>\t%s\n\tLine\t\t=>\t%d", __func__, __LINE__);
    if ([self.petAttribute.text isEqualToString:@""]) {
        [self.petAttribute becomeFirstResponder];
        return NO;
    }
    
    
    [textField resignFirstResponder];
    return YES;
}


@end

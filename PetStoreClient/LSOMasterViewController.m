//
//  LSOMasterViewController.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "LSOMasterViewController.h"
#import "LSOPet.h"
#import "Pet.h"
#import "LSODetailViewController.h"

@interface LSOMasterViewController () {
    NSMutableArray *_objects;
}
@end

@implementation LSOMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.pets addObserver:self
                    forKeyPath:@"dataLoaded"
                       options:NSKeyValueObservingOptionNew
                       context:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"Number of rowsInSection=%d",[self.pets count]);
    return [self.pets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    Pet *pet = [self.pets petAtIndex:indexPath.row];
    cell.textLabel.text = pet.petName;
    return cell;

}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.pets removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"editPet"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Pet *pet = [self.pets petAtIndex:indexPath.row];
        LSODetailViewController *destination = (LSODetailViewController *)[segue destinationViewController];
        destination.selectedPet = pet;
        destination.pets = self.pets;
        destination.selectedPetIndex = [[self.tableView indexPathForSelectedRow] row];
    }
    else if ([[segue identifier] isEqualToString:@"addPet"]) {
        LSODetailViewController *destination = (LSODetailViewController *)[segue destinationViewController];
        destination.pets = self.pets;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    NSLog(@"%@",object);
    if ([keyPath isEqualToString:@"dataLoaded"] )
    {
        if (self.pets.dataLoaded == YES)
        {
            NSLog(@"reload data");
            [self.tableView reloadData];
        }
        else
        {
            NSLog(@"No");
        }
    }
}

@end

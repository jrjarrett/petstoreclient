//
//  LSODetailViewController.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Pet;
@class LSOPet;

@interface LSODetailViewController : UITableViewController<UITextFieldDelegate>

@property (weak,nonatomic) Pet *selectedPet;
@property (weak,nonatomic) LSOPet* pets;
@property (assign,nonatomic) NSUInteger selectedPetIndex;

@end

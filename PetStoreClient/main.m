//
//  main.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LSOAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LSOAppDelegate class]));
    }
}

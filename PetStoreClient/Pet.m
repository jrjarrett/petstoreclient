//
//  Pet.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "Pet.h"

@implementation Pet

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary
{
    self = [self init];
    
    if (self) {
        _petId = (NSUInteger)[jsonDictionary[@"petId"] integerValue ];
        _petTypeId = (NSUInteger)[jsonDictionary[@"petTypeId"] integerValue];
        _petName = jsonDictionary[@"name"];
        _age = (NSUInteger)[jsonDictionary[@"age"] integerValue];
        _weight = (NSUInteger)[jsonDictionary[@"weight"] integerValue];
        
        NSNumber *n  = jsonDictionary[@"readyForAdoption"] ;
        _readyForAdoption = [n boolValue];
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *price =jsonDictionary[@"adoptionPrice"];
        NSLog(@"The value is %f",[price floatValue]);
        _adoptionPrice = price;
        _furColor = jsonDictionary[@"furColor"];
        n  = jsonDictionary[@"likesKids"];
        _likesKids = [n boolValue];
        _petType = jsonDictionary[@"petType"];
    }
    
    return self;
}

- (id)initWithName:(NSString *)name
            andPetTypeId:(NSUInteger)petTypeId
            andAge:(NSUInteger)age
            andWeight:(NSUInteger)weight
            andReadyForAdoption:(BOOL)readyForAdoption
            andPrice:(NSNumber *)price
            andFurColor:(NSString *)color
            andLikesKids:(BOOL)likesKids
{
    self = [self init];
    
    if (self) {
        _petId = 0;
        _petTypeId = petTypeId;
        _petName = name;
        _age = age;
        _weight = weight;
        _readyForAdoption = readyForAdoption;
        _adoptionPrice = price;
        _furColor = color;
        _likesKids = likesKids;
        _petType = @"";
    }
    
    return self;
}

- (NSDictionary *)jsonDictionary
{
    NSDictionary *json = @{
    @"petId" : [NSString stringWithFormat: @"%d", [self petId]],
    @"petTypeId" : [NSString stringWithFormat: @"%d",[self petTypeId]],
    @"name" : [self petName],
    @"age" : [NSString stringWithFormat: @"%d",[self age]],
    @"weight" : [NSString stringWithFormat: @"%d",[self weight]],
    @"readyForAdoption" : ([self readyForAdoption] ? @"true" : @"false"),
    @"adoptionPrice" : [NSString stringWithFormat: @"%f",[[self adoptionPrice] doubleValue]],
    @"furColor" : [self furColor],
    @"likesKids" : ([self likesKids] ? @"true" : @"false"),
    @"petType" : [self petType]
    };
    
    return json;
    
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<Pet: id %d, petTypeId %d, petName %@, age %d, weight %d readyForAdoption %d, adoptionPrice %@, furColor %@, likesKids %d>",
            [self petId], [self petTypeId], [self petName], [self age], [self weight], [self readyForAdoption], [self adoptionPrice], [self furColor], [self likesKids]];
}

@end

//
//  Pet.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pet : NSObject

/*
 *Unique identifier for a pet
 */
@property (assign) NSUInteger petId;

/**
 *Unique identifier for a pet type.
 *Cannot be null.
 */
@property (assign) NSUInteger petTypeId;

/**
 *Pet name
 */
@property (strong) NSString *petName;

/**
 *Pet age
 */
@property (assign) NSUInteger age;

/**
 *Pet weight
 */
@property (assign) NSUInteger weight;

/**
 *Whether or not the pet is ready for adoption
 */
@property (assign) BOOL readyForAdoption;

/*
 *Price of adoption
 */
@property (strong) NSNumber *adoptionPrice;

/**
 *Fur color of pet
 */
@property (strong) NSString *furColor;

/**
 *Whether or not the pet likes kids
 */
@property (assign) BOOL likesKids;

/**
 *The pet type description
 */
@property (strong) NSString *petType;

- (id)initWithJSONDictionary:(NSDictionary *)jsonDictionary;
- (NSDictionary *)jsonDictionary;
- (id)initWithName:(NSString *)name
      andPetTypeId:(NSUInteger)petTypeId
            andAge:(NSUInteger)age
         andWeight:(NSUInteger)weight
andReadyForAdoption:(BOOL)readyForAdoption
          andPrice:(NSNumber *)price
       andFurColor:(NSString *)color
      andLikesKids:(BOOL)likesKids;

@end

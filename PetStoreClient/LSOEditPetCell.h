//
//  LSOEditPetCell.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSOEditPetCell : UITableViewCell

@property (strong,nonatomic) IBOutlet UITextField *petAttribute;
@property (strong,nonatomic) IBOutlet UILabel *petAttributeLabel;
@property (strong,nonatomic) NSString *petMethod;

@end

//
//  LSOAddPetController.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "LSOAddPetController.h"
#import "Pet.h"
#import "LSOPet.h"
#import "LSOEditPetCell.h"

@interface LSOAddPetController ()
@property (weak, nonatomic) IBOutlet UITextField *petTypeId;
@property (weak, nonatomic) IBOutlet UITextField *petName;
@property (weak, nonatomic) IBOutlet UITextField *petAge;
@property (weak, nonatomic) IBOutlet UITextField *petWeight;
@property (weak, nonatomic) IBOutlet UITextField *petReadyForAdoption;
@property (weak, nonatomic) IBOutlet UITextField *petAdoptionPrice;
@property (weak, nonatomic) IBOutlet UITextField *petFurColor;
@property (weak, nonatomic) IBOutlet UITextField *petLikesKids;

@end

@implementation LSOAddPetController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    static NSString *CellIdentifier = @"petCell";
//    LSOEditPetCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    if (cell == nil) {
//        NSLog(@"\n\tFunction\t=>\t%s\n\tLine\t\t=>\t%d", __func__, __LINE__);
//        cell = [[LSOEditPetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//    }
//    
//    [self configureCell:cell atIndexPath:indexPath];
//    
//    return cell;
//}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark UITextView delegates
// called when textField start editting.
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGFloat newOrigin = 0.0;
    if (textField == self.petTypeId || textField == self.petName )
        newOrigin = 0.0;
    if (textField ==self.petAge || textField == self.petWeight || textField ==self.petReadyForAdoption || textField ==self.petAdoptionPrice  )
        newOrigin = -100.0;
    if ( textField == self.petFurColor || textField ==self.petLikesKids)
        newOrigin = -120.0;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = newOrigin;
    [self.view setFrame:frame];
    [UIView commitAnimations];
    
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self putBackTextFields];
    return YES;
}
- (void)putBackTextFields
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.35f];
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [self.view setFrame:frame];
    [UIView commitAnimations];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if ([self.petTypeId.text isEqualToString:@""]) {
        [self.petTypeId becomeFirstResponder];
        return NO;
    }
    if ([self.petName.text isEqualToString:@""]) {
        [self.petName becomeFirstResponder];
        return NO;
    }
    if ([self.petAdoptionPrice.text isEqualToString:@""]) {
        [self.petAdoptionPrice becomeFirstResponder];
        return NO;
    }
    if ([self.petAge.text isEqualToString:@""]) {
        [self.petAge becomeFirstResponder];
        return NO;
    }
    if ([self.petFurColor.text isEqualToString:@""]) {
        [self.petFurColor becomeFirstResponder];
        return NO;
    }
    if ([self.petLikesKids.text isEqualToString:@""]) {
        [self.petLikesKids becomeFirstResponder];
        return NO;
    }
    if ([self.petReadyForAdoption.text isEqualToString:@""]) {
        [self.petReadyForAdoption becomeFirstResponder];
        return NO;
    }
    if ([self.petWeight.text isEqualToString:@""]) {
        [self.petWeight becomeFirstResponder];
        return NO;
        
    }
    
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        
        NSUInteger petTypeId = [self.petTypeId.text integerValue];
        NSUInteger age = [self.petAge.text  integerValue];
        NSUInteger weight = [self.petWeight.text  integerValue];
        BOOL readyForAdpotion = [self.petReadyForAdoption.text isEqualToString:@"true"] ? YES : NO;
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *price = [f numberFromString:self.petAdoptionPrice.text];
        BOOL likesKids = [self.petLikesKids.text isEqualToString:@"true"] ? YES : NO;
        
        
        Pet* pet = [[Pet alloc] initWithName:self.petName.text andPetTypeId:petTypeId andAge:age andWeight:weight andReadyForAdoption:readyForAdpotion andPrice:price andFurColor:self.petFurColor.text andLikesKids:likesKids];
        
        [self.pets addPet:pet];
    
    
    [self putBackTextFields];
    [textField resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];

    return YES;
    
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

//- (void)configureCell:(LSOEditPetCell *)cell atIndexPath:(NSIndexPath *)indexPath
//{
//    
//    
//    if (indexPath.section == 0) {
//        if (indexPath.row == 0) {
//            cell.petAttributeLabel.text = @"Name";
//            cell.petMethod = @"selectedPet";
//        }
//        else if (indexPath.row == 1) {
//            cell.petAttributeLabel.text = @"Age";
//            cell.petMethod = @"age";
//            
//        }
//        else if (indexPath.row == 2) {
//            cell.petAttributeLabel.text = @"Weight";
//            cell.petMethod = @"weight";
//            
//        }
//        else if (indexPath.row == 3) {
//            cell.petAttributeLabel.text = @"Ready For Adoption?";
//            cell.petMethod = @"readyForAdoption";
//            
//        }
//        else if (indexPath.row == 4) {
//            cell.petAttributeLabel.text = @"Adoption Price";
//            cell.petMethod = @"adoptionPrice";
//            
//        }
//        else if (indexPath.row == 5) {
//            cell.petAttributeLabel.text = @"Fur Color";
//            cell.petMethod = @"furColor";
//            
//        }
//        else if (indexPath.row == 6) {
//            cell.petAttributeLabel.text = @"Likes Kids?";
//            cell.petMethod = @"likesKids";
//            
//        }
//        else if (indexPath.row == 7) {
//            cell.petAttributeLabel.text = @"Pet Type";
//            cell.petMethod = @"petType";
//            
//        }
//        else {
//            cell.petAttributeLabel.text = @"TBD";
//            
//        }
//    }
//    
//}
//- (IBAction)saveTapped:(id)sender {
//    
//    NSLog(@"saveTapped");
//    
//    self.selectedPet = [[Pet alloc] init];
//    
//    for (int i=0; i < 8;i++) {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//        LSOEditPetCell *cell = (LSOEditPetCell*)[self.tableView cellForRowAtIndexPath:indexPath];
//        NSLog(@"%@",cell.petAttributeLabel.text);
//        if (indexPath.row == 0) {
//            
//            self.selectedPet.petName = [NSString stringWithString:cell.petAttribute.text];
//        }
//        else if (indexPath.row == 1) {
//            self.selectedPet.age = [cell.petAttribute.text integerValue];
//            
//        }
//        else if (indexPath.row == 2) {
//            self.selectedPet.weight = [cell.petAttribute.text integerValue];
//            
//        }
//        else if (indexPath.row == 3) {
//            self.selectedPet.readyForAdoption = [cell.petAttribute.text isEqualToString:@"1"] ? YES : NO;
//            
//        }
//        else if (indexPath.row == 4) {
//            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
//            [f setNumberStyle:NSNumberFormatterDecimalStyle];
//            self.selectedPet.adoptionPrice  = [f numberFromString:cell.petAttribute.text];
//            
//        }
//        else if (indexPath.row == 5) {
//            self.selectedPet.furColor = [NSString stringWithString:cell.petAttribute.text ];
//            
//        }
//        else if (indexPath.row == 6) {
//            self.selectedPet.likesKids = [cell.petAttribute.text isEqualToString:@"1"] ? YES : NO;
//            
//        }
//        else if (indexPath.row == 7) {
//            self.selectedPet.petTypeId = [cell.petAttribute.text integerValue];
//            self.selectedPet.petType = [NSString stringWithFormat:@"%@",@"empty"];
//            
//        }
//    }
//}



@end

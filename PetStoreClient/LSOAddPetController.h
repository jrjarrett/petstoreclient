//
//  LSOAddPetController.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Pet;
@class LSOPet;

@interface LSOAddPetController : UITableViewController<UITextFieldDelegate>

@property (strong,nonatomic) Pet *selectedPet;
@property (strong, nonatomic) LSOPet *pets;

@end

//
//  LSODetailViewController.m
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/5/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import "LSODetailViewController.h"
#import "Pet.h"
#import "LSOPet.h"
#import "LSOEditPetCell.h"

@interface LSODetailViewController ()

@end

@implementation LSODetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"petCell";
    LSOEditPetCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSLog(@"\n\tFunction\t=>\t%s\n\tLine\t\t=>\t%d", __func__, __LINE__);
        cell = [[LSOEditPetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
  
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

- (void)configureCell:(LSOEditPetCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.petAttributeLabel.text = @"Name";
            cell.petAttribute.text = self.selectedPet.petName;
            cell.petMethod = @"selectedPet";
        }
        else if (indexPath.row == 1) {
            cell.petAttributeLabel.text = @"Age";
            cell.petAttribute.text = [NSString stringWithFormat:@"%d", self.selectedPet.age];
            cell.petMethod = @"age";

        }
        else if (indexPath.row == 2) {
            cell.petAttributeLabel.text = @"Weight";
            cell.petAttribute.text = [NSString stringWithFormat:@"%d",self.selectedPet.weight ];
            cell.petMethod = @"weight";

        }
        else if (indexPath.row == 3) {
            cell.petAttributeLabel.text = @"Ready For Adoption?";
            cell.petAttribute.text = [NSString stringWithFormat:@"%d",self.selectedPet.readyForAdoption ];
            cell.petMethod = @"readyForAdoption";

        }
         else if (indexPath.row == 4) {
            cell.petAttributeLabel.text = @"Adoption Price";
            cell.petAttribute.text = [NSString stringWithFormat:@"%@",self.selectedPet.adoptionPrice ];
             cell.petMethod = @"adoptionPrice";

        }
        else if (indexPath.row == 5) {
            cell.petAttributeLabel.text = @"Fur Color";
            cell.petAttribute.text = self.selectedPet.furColor;
            cell.petMethod = @"furColor";

        }
        else if (indexPath.row == 6) {
            cell.petAttributeLabel.text = @"Likes Kids?";
            cell.petAttribute.text = [NSString stringWithFormat:@"%d",self.selectedPet.likesKids ];
            cell.petMethod = @"likesKids";

        }
        else if (indexPath.row == 7) {
            cell.petAttributeLabel.text = @"Pet Type";
            cell.petAttribute.text = self.selectedPet.petType;
            cell.petMethod = @"petType";

        }
        else {
            cell.petAttributeLabel.text = @"TBD";
            
        }
    }
 
}
- (IBAction)saveTapped:(id)sender {
    
    NSLog(@"saveTapped");

    
    for (int i=0; i < 8;i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        LSOEditPetCell *cell = (LSOEditPetCell*)[self.tableView cellForRowAtIndexPath:indexPath];
        NSLog(@"%@",cell.petAttributeLabel.text);
        if (indexPath.row == 0) {
            
            self.selectedPet.petName = cell.petAttribute.text;
        }
        else if (indexPath.row == 1) {
            self.selectedPet.age = [cell.petAttribute.text integerValue];
            
        }
        else if (indexPath.row == 2) {
            self.selectedPet.weight = [cell.petAttribute.text integerValue];
            
        }
        else if (indexPath.row == 3) {
            self.selectedPet.readyForAdoption = [cell.petAttribute.text isEqualToString:@"1"] ? YES : NO;
            
        }
        else if (indexPath.row == 4) {
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
            self.selectedPet.adoptionPrice  = [f numberFromString:cell.petAttribute.text];
            
        }
        else if (indexPath.row == 5) {
            self.selectedPet.furColor = cell.petAttribute.text;
            
        }
        else if (indexPath.row == 6) {
            self.selectedPet.likesKids = [cell.petAttribute.text isEqualToString:@"1"] ? YES : NO;
            
        }
        else if (indexPath.row == 7) {
            self.selectedPet.petType = cell.petAttribute.text;
            
        }
    }

    NSLog(@"Updating pet %@ - %d",self.selectedPet.petName,self.selectedPet.petId);
    NSString *putURL = [NSString stringWithFormat:@"http://localhost:8080/service/pet/%d",self.selectedPet.petId];
    NSURL *url = [NSURL URLWithString:putURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"PUT";
    
    NSLog(@"%@",[self.selectedPet jsonDictionary]);
    NSError *jsonError;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[self.selectedPet jsonDictionary]
                                                   options:NSJSONReadingMutableContainers
                                                     error:&jsonError];
    request.HTTPBody = data;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
  
    
    NSOperationQueue *dataQueue = [[NSOperationQueue alloc] init];
    dataQueue.name = @"PutQueue";
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:dataQueue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               
                               if ([data length] >0 && error == nil)
                               {
                                   
                                   // DO YOUR WORK HERE
                                   NSLog(@"Doing work");
                                   
                               }
                               else if ([data length] == 0 && error == nil)
                               {
                                   NSLog(@"Nothing was downloaded.");
                               }
                               else if (error != nil){
                                   NSLog(@"Error = %@", error);
                               }
                           }];
    [self.navigationController popViewControllerAnimated:YES];
}



@end

//
//  LSOMasterViewController.h
//  PetStoreClient
//
//  Created by Jim Jarrett on 12/3/12.
//  Copyright (c) 2012 Jim Jarrett. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LSOPet;

@interface LSOMasterViewController : UITableViewController

@property (strong) LSOPet *pets;
@end
